<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\soalController;
use App\Http\Controllers\ujianController;

Route::get('/soal/index', [soalController::class, 'index'])->name('soal-index');
Route::post('/soal/tambah', [soalController::class, 'tbsoal'])->name('tbsoal');
Route::get('/soal/delete/{id}', [soalController::class, 'dlsoal']);
// Route::get('/soal/detail/{id}', [kunciController::class, 'dtsoal']);

Route::get('/ujian/index', [ujianController::class, 'index'])->name('ujian-index');
Route::get('/ujian/delete/{id}', [ujianController::class, 'dlujian']);

Route::get('/', function () {
    return view('landing');
});