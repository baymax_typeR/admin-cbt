@extends('layout-admin.main')

@section('title', 'Admin | Data Kunci')

@section('content')

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" style="background-color: #FFFFFF">
                        <strong class="card-title">Data Kunci
                            <a title="Tambah Data Kunci Jawaban" data-toggle="modal" data-target="#tambahData" style="color: white; float: right;"
                                class="btn btn-primary btn-sm tbkunci"><i class="fa fa-plus"></i>&nbsp;
                                Tambah Data</a>
                        </strong>
                    </div>
                    <div class="card-body">
                        <table id="bootstrap-data-table-export" class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>No.</th>                                    
                                    <th>Soal</th>
                                    <th>Kunci Jawaban</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($kunci as $data)
                                <tr>
                                    <td style="text-align: center; width: 8px;">{{ $loop->iteration }}</td>                                    
                                    <td>{{ $data->soal }}</td>
                                    <td>{{ $data->KUNCI_JWB }}</td>
                                    <td style="text-align: center; color: white;">
                                        <a style="width: 45px;" data-id_kunci="{{ $data->ID_KUNCI }}"
                                            data-id_soal="{{ $data->ID_SOAL }}" data-kunci_jwb="{{ $data->KUNCI_JWB }}"
                                            data-target="#tambahData" data-toggle="modal"
                                            class="btn btn-success edkunci" title="Ubah Data Kunci"><i
                                                class="fa fa-pencil"></i></a>
                                        <a style="width: 45px;" data-id_kunci="{{ $data->ID_KUNCI }}"
                                            class="btn btn-danger dlkunci" title="Hapus Data Kunci"><i
                                                class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No.</th>                                    
                                    <th>Soal</th>
                                    <th>Kunci Jawaban</th>
                                    <th>Aksi</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                &emsp; Note: jika kunci soal N sudah ada, maka tidak bisa buat kunci untuk soal N lagi
            </div>
        </div>
    </div><!-- .animated -->
</div><!-- .content -->

<!-- modal -->
<!-- modal tambah data -->
<div class="modal fade" id="tambahData" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true"
    data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form id="formPegawai" name="formPegawai">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="largeModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span id="ef5" aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mr-4 ml-4">
                    <input id="action" hidden type="text" name="action" class="form-control" value="">
                    <input id="ID_KUNCI" hidden type="text" name="ID_KUNCI" class="form-control" value="">
                    <div class="row form-group">
                        <div class="col col-md-2"><label for="SOAL" class="form-control-label">Soal</label></div>
                        <div class="col-12 col-md-10">
                            <select id="ID_SOAL" class="form-control" name="ID_SOAL" onchange="proses()">
                                <option disabled selected>--- Pilih Soal ---</option>
                                @foreach($soal as $data)
                                <option value="{{ $data->ID_SOAL }}">{{ $data->SOAL }} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-2"><label for="KUNCI_JWB" class="form-control-label">Kunci
                                Jawaban</label>
                        </div>
                        <div class="col-12 col-md-10">
                            <div class="form-check">
                                <div class="radio">
                                    <label for="JWB_A" class="form-check-label ">
                                        <input type="radio" id="JWB_A" name="RADIOS" value="" class="form-check-input">
                                        <p id="vJWB_A"></p>
                                    </label>
                                </div>
                                <div class="radio">
                                    <label for="JWB_B" class="form-check-label ">
                                        <input type="radio" id="JWB_B" name="RADIOS" value="" class="form-check-input">
                                        <p id="vJWB_B"></p>
                                    </label>
                                </div>
                                <div class="radio">
                                    <label for="JWB_C" class="form-check-label ">
                                        <input type="radio" id="JWB_C" name="RADIOS" value="" class="form-check-input">
                                        <p id="vJWB_C"></p>
                                    </label>
                                </div>
                                <div class="radio">
                                    <label for="JWB_D" class="form-check-label ">
                                        <input type="radio" id="JWB_D" name="RADIOS" value="" class="form-check-input">
                                        <p id="vJWB_D"></p>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btnF5" data-dismiss="modal"><i
                            class="fa fa-reply"></i>&nbsp; Kembali</button>
                    <button id="submit" type="submit" class="btn btn-success"><i class="fa fa-save"></i>&nbsp;
                        Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- .modal -->

<script>
    function proses() {
        var id = document.getElementById("ID_SOAL").value;

        $.ajax({
            url: "/soal/detail/" + id,
            type: "GET",
            dataType: 'json',
            success: function (data) {
                document.getElementById("JWB_A").value = data[0].JWB_A;
                document.getElementById("JWB_B").value = data[0].JWB_B;
                document.getElementById("JWB_C").value = data[0].JWB_C;
                document.getElementById("JWB_D").value = data[0].JWB_D;
                $('#vJWB_A').html(data[0].JWB_A);
                $('#vJWB_B').html(data[0].JWB_B);
                $('#vJWB_C').html(data[0].JWB_C);
                $('#vJWB_D').html(data[0].JWB_D);
            }
        });
    }

    $('.tbkunci').click(function () {
        $('#largeModalLabel').html('Tambah Data Kunci');
        $('#action').val('tambah');
        $('#tambahData').show();
    });

    $('.edkunci').click(function () {
        $('#largeModalLabel').html('Ubah Data Kunci');
        $('#action').val('edit');
        $('#ID_KUNCI').val($(this).data('id_kunci'))
        $('#ID_SOAL').val($(this).data('id_soal'))
        $('#tambahData').show();

        var id = document.getElementById("ID_SOAL").value;

        $.ajax({
            url: "/soal/detail/" + id,
            type: "GET",
            dataType: 'json',
            success: function (data) {
                document.getElementById("JWB_A").value = data[0].JWB_A;
                document.getElementById("JWB_B").value = data[0].JWB_B;
                document.getElementById("JWB_C").value = data[0].JWB_C;
                document.getElementById("JWB_D").value = data[0].JWB_D;
                $('#vJWB_A').html(data[0].JWB_A);
                $('#vJWB_B').html(data[0].JWB_B);
                $('#vJWB_C').html(data[0].JWB_C);
                $('#vJWB_D').html(data[0].JWB_D);
            }
        });
    });

    $('#submit').click(function (e) {
        e.preventDefault();
        $(this).html('Sending...');
        $.ajax({
            data: $('#formPegawai').serialize(),
            url: "{{ route('tbkunci') }}",
            type: "POST",
            dataType: 'json',
            success: function (data) {
                $('#formPegawai').trigger('reset');
                $('#submit').html('Simpan');
                $('#tambahData').hide();
                console.log('Sukses: ', data);

                Swal.fire({
                    title: 'Success!',
                    text: 'Your data has been updated.',
                    type: 'success',
                    showConfirmButton: true,
                }).then((result) => {
                    if (result.value) {
                        location.reload(true);
                    } else {
                        location.reload(true);
                    }
                });
            },
            error: function (data) {
                console.log('Error: ', data);
            }
        });
    });

    $(document).on('click', '.dlkunci', function (e) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#29A746',
            cancelButtonColor: '#DC3646',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {

                var id = $(this).data('id_kunci');

                $.ajax({
                    url: "{{ url('/kunci/delete') }}/" + id,
                    type: "GET",
                    dataType: 'json',
                    success: function (data) {
                        console.log('Sukses: ', data);
                    },
                    error: function (data) {
                        console.log('Error: ', data);
                    }
                });

                Swal.fire({
                    title: 'Deleted!',
                    text: 'Your data has been deleted.',
                    type: 'success',
                    showConfirmButton: true,
                }).then((result) => {
                    if (result.value) {
                        location.reload(true);
                    } else {
                        location.reload(true);
                    }
                });
            } else {
                swal.close();
            }
        });
    });
</script>

@endsection