@extends('layout-admin.main')

@section('title', 'Admin | Data Ujian')

@section('content')

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" style="background-color: #FFFFFF">
                        <strong class="card-title">Data Ujian</strong>
                    </div>
                    <div class="card-body">
                        <table id="bootstrap-data-table-export" class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Id User</th>
                                    <th>Username</th>
                                    <th>Nama</th>
                                    <th>Waktu Login</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $dataUjian)
                                <tr>
                                    <td style="width: 8px; text-align: center;">{{ $loop->iteration }}</td>
                                    <td>{{ $dataUjian['ID_USER'] }}</td>
                                    <td>{{ $dataUjian['username'] }}</td>
                                    <td>{{ $dataUjian['name'] }}</td>
                                    <td>{{ tgl_full($dataUjian['CREATED_AT'], 1) }}</td>
                                    <td style="text-align: center; color: white;">                                        
                                        <a style="width: 50px;" data-id_ujian="{{ $dataUjian['ID_UJIAN'] }}"
                                            class="btn btn-danger dlujian" title="Hapus Data Ujian"><i
                                                class="fa fa-trash-o"></i></a>
                                        <a style="width: 50px;" class="btn btn-secondary dtujian"
                                            title="Detail Hasil Ujian"><i class="fa fa-info"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No.</th>
                                    <th>Id User</th>
                                    <th>Username</th>
                                    <th>Nama</th>
                                    <th>Waktu Login</th>
                                    <th>Aksi</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <!-- &emsp; Note:  -->
            </div>
        </div>
    </div><!-- .animated -->
</div><!-- .content -->

<!-- modal -->
<!-- modal detail -->

<!-- .modal -->

<script>
    $(document).on('click', '.dlujian', function (e) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#29A746',
            cancelButtonColor: '#DC3646',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {

                var id = $(this).data('id_ujian');

                $.ajax({
                    url: "{{ url('/ujian/delete') }}/" + id,
                    type: "GET",
                    dataType: 'json',
                    success: function (data) {
                        console.log('Sukses: ', data);
                    },
                    error: function (data) {
                        console.log('Error: ', data);
                    }
                });

                Swal.fire({
                    title: 'Deleted!',
                    text: 'Your data has been deleted.',
                    type: 'success',
                    showConfirmButton: true,
                }).then((result) => {
                    if (result.value) {
                        location.reload(true);
                    } else {
                        location.reload(true);
                    }
                });
            } else {
                swal.close();
            }
        });
    });
</script>

@endsection