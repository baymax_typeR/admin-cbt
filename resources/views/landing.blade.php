@extends('layout-admin.main')

@section('title', 'Admin | Landing')

@section('content')

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">                
                    <div class="card-body">
                        <h3>Landing page!</h3>
                    </div>
                </div>
                <!-- &emsp; Note:  -->
            </div>
        </div>
    </div><!-- .animated -->
</div><!-- .content -->

@endsection