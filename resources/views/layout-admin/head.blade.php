<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>@yield('title')</title>

<!-- <meta name="description" content="Sufee Admin - HTML5 Admin Template"> -->
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="apple-touch-icon" href="apple-icon.png">
<link rel="shortcut icon" href="favicon.ico">

<link rel="stylesheet" href="{{asset('assets-admin/folder-vendors/bootstrap/dist/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('assets-admin/folder-vendors/font-awesome/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{asset('assets-admin/folder-vendors/themify-icons/css/themify-icons.css')}}">
<link rel="stylesheet" href="{{asset('assets-admin/folder-vendors/flag-icon-css/css/flag-icon.min.css')}}">
<link rel="stylesheet" href="{{asset('assets-admin/folder-vendors/selectFX/css/cs-skin-elastic.css')}}">
<link rel="stylesheet" href="{{asset('assets-admin/folder-vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('assets-admin/folder-vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css')}}">

<link rel="stylesheet" href="{{asset('assets-admin/folder-assets/css/style.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.min.css">

<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

<script src="{{asset('assets-admin/folder-vendors/jquery/dist/jquery.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.min.js"></script>

<script>
    $(document).ready(function () {

        // Refresh page
        $("#ef5").click(function () {
            location.reload();
        });

        $(".btnF5").click(function () {
            location.reload();
        });

    });
</script>