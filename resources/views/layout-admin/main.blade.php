<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en">
<!--<![endif]-->

<head>
    @include('layout-admin.head')
</head>

<body >

    <!-- Left Panel -->
    <aside id="left-panel" class="left-panel">
        @include('layout-admin.sidebar')
    </aside><!-- /#left-panel -->
    <!-- Left Panel -->

    <!-- Right Panel -->
    
    <div id="right-panel" class="right-panel">

        <!-- Header-->
        @include('layout-admin.navbar')
        <!-- Header-->

        @include('layout-admin.breadcrumbs')

        @yield('content')        

    </div><!-- /#right-panel -->

    <!-- Right Panel -->

    @include('layout-admin.script')

</body>

</html>
