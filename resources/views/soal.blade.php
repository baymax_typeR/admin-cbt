@extends('layout-admin.main')

@section('title', 'Admin | Data Soal')

@section('content')

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" style="background-color: #FFFFFF">
                        <strong class="card-title">Data Soal
                            <a title="Tambah Data Soal" data-toggle="modal" data-target="#tambahData"
                                style="color: white; float: right;" class="btn btn-primary btn-sm tbsoal"><i
                                    class="fa fa-plus"></i>&nbsp;
                                Tambah Data</a>
                            <!-- <a title="Tambah Kunci" data-toggle="modal" data-target="#tambahData"
                                style="color: white; float: right;" class="btn btn-info btn-sm tbsoal"><i
                                    class="fa fa-key"></i></a> -->
                        </strong>
                    </div>
                    <div class="card-body">
                        <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Soal</th>
                                    <th>Kunci</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($soal as $data)
                                <tr>
                                    <td style="width: 8px; text-align: center;">{{ $loop->iteration }}</td>
                                    <td>{{ $data->SOAL }}</td>
                                    <td>{{ $data->KUNCI }}</td>
                                    <td style="text-align: center; color: white;">
                                        <a style="width: 50px;" data-id_soal="{{ $data->ID_SOAL }}"
                                            data-soal="{{ $data->SOAL }}" data-jwb_a="{{ $data->JWB_A }}"
                                            data-jwb_b="{{ $data->JWB_B }}" data-jwb_c="{{ $data->JWB_C }}"
                                            data-jwb_d="{{ $data->JWB_D }}" data-status="{{ $data->STATUS }}"
                                            data-target="#tambahData" data-toggle="modal" class="btn btn-success edsoal"
                                            title="Ubah Data"><i class="fa fa-pencil"></i></a>
                                        <a style="width: 50px;" data-id_soal="{{ $data->ID_SOAL }}"
                                            data-soal="{{ $data->SOAL }}" data-jwb_a="{{ $data->JWB_A }}"
                                            data-jwb_b="{{ $data->JWB_B }}" data-jwb_c="{{ $data->JWB_C }}"
                                            data-jwb_d="{{ $data->JWB_D }}" data-status="{{ $data->STATUS }}"
                                            data-target="#tambahData" data-toggle="modal"
                                            class="btn btn-warning edkunci" title="Ubah Kunci"><i
                                                class="fa fa-key"></i></a>
                                        <a style="width: 50px;" data-id_soal="{{ $data->ID_SOAL }}"
                                            class="btn btn-danger dlsoal" title="Hapus Data"><i
                                                class="fa fa-trash-o"></i></a>
                                        <a style="width: 50px;" data-id_soal="{{ $data->ID_SOAL }}"
                                            data-soal="{{ $data->SOAL }}" data-jwb_a="{{ $data->JWB_A }}"
                                            data-jwb_b="{{ $data->JWB_B }}" data-jwb_c="{{ $data->JWB_C }}"
                                            data-jwb_d="{{ $data->JWB_D }}" data-kunci="{{ $data->KUNCI }}"
                                            data-target="#detailData" data-toggle="modal"
                                            class="btn btn-secondary dtsoal" title="Detail Data"><i
                                                class="fa fa-info"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No.</th>
                                    <th>Soal</th>
                                    <th>Kunci</th>
                                    <th>Aksi</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                &emsp; Note: jika soal ada di soal dan blm ada di kunci, maka soal di soal tdk bisa diupdate
            </div>
        </div>
    </div><!-- .animated -->
</div><!-- .content -->

<!-- modal -->
<!-- modal tambah data -->
<div class="modal fade" id="tambahData" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true"
    data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form id="formPegawai" name="formPegawai">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="largeModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span id="ef5" aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mr-4 ml-4">
                    <input id="action" hidden type="text" name="action" class="form-control" value="">
                    <input id="ID_SOAL" hidden type="text" name="ID_SOAL" class="form-control" value="">
                    <input id="STATUS" hidden type="text" name="STATUS" class="form-control" value="">
                    <div class="row form-group">
                        <div class="col col-md-2"><label for="SOAL" class="form-control-label">Soal</label></div>
                        <div class="col-12 col-md-10"><textarea name="SOAL" id="SOAL" rows="3"
                                placeholder="Tulis soal disini..." class="form-control"></textarea></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-2"><label for="JWB_A" class="form-control-label">Jawaban
                                A</label>
                        </div>
                        <div class="col-12 col-md-10"><textarea name="JWB_A" id="JWB_A" rows="3"
                                placeholder="Tulis jawaban A disini..." class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-2"><label for="JWB_B" class="form-control-label">Jawaban
                                B</label>
                        </div>
                        <div class="col-12 col-md-10"><textarea name="JWB_B" id="JWB_B" rows="3"
                                placeholder="Tulis jawaban B disini..." class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-2"><label for="JWB_C" class="form-control-label">Jawaban
                                C</label>
                        </div>
                        <div class="col-12 col-md-10"><textarea name="JWB_C" id="JWB_C" rows="3"
                                placeholder="Tulis jawaban C disini..." class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-2"><label for="JWB_D" class="form-control-label">Jawaban
                                D</label>
                        </div>
                        <div class="col-12 col-md-10"><textarea name="JWB_D" id="JWB_D" rows="3"
                                placeholder="Tulis jawaban D disini..." class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btnF5" data-dismiss="modal"><i
                            class="fa fa-reply"></i>&nbsp; Kembali</button>
                    <button id="submit" type="submit" class="btn btn-success"><i class="fa fa-save"></i>&nbsp;
                        Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- modal detail -->
<div class="modal fade" id="detailData" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true"
    data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form id="formPegawai" name="formPegawai">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="detailModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mr-4 ml-4">
                    <input id="dtID_SOAL" hidden type="text" name="ID_SOAL" class="form-control" value="">
                    <div class="row form-group">
                        <div class="col col-md-2"><label for="SOAL" class="form-control-label">Soal</label></div>
                        <div class="col-12 col-md-10">
                            <p style="color: #000000;" name="SOAL" id="dtSOAL"></p>
                        </div>
                    </div>
                    <hr>
                    <div class="row form-group">
                        <div class="col col-md-2"><label for="JWB_A" class="form-control-label">Jawaban
                                A</label>
                        </div>
                        <div class="col-12 col-md-10">
                            <p style="color: #000000;" name="JWB_A" id="dtJWB_A"></p>
                        </div>
                    </div>
                    <hr>
                    <div class="row form-group">
                        <div class="col col-md-2"><label for="JWB_B" class="form-control-label">Jawaban
                                B</label>
                        </div>
                        <div class="col-12 col-md-10">
                            <p style="color: #000000;" name="JWB_B" id="dtJWB_B"></p>
                        </div>
                    </div>
                    <hr>
                    <div class="row form-group">
                        <div class="col col-md-2"><label for="JWB_C" class="form-control-label">Jawaban
                                C</label>
                        </div>
                        <div class="col-12 col-md-10">
                            <p style="color: #000000;" name="JWB_C" id="dtJWB_C"></p>
                        </div>
                    </div>
                    <hr>
                    <div class="row form-group">
                        <div class="col col-md-2"><label for="JWB_D" class="form-control-label">Jawaban
                                D</label>
                        </div>
                        <div class="col-12 col-md-10">
                            <p style="color: #000000;" name="JWB_D" id="dtJWB_D"></p>
                        </div>
                    </div>
                    <hr>
                    <div class="row form-group">
                        <div class="col col-md-2"><label for="KUNCI" class="form-control-label">Kunci</label>
                        </div>
                        <div class="col-12 col-md-10">
                            <p style="color: #000000;" name="KUNCI" id="dtKUNCI"></p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i
                            class="fa fa-reply"></i>&nbsp; Kembali</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- .modal -->

<script>
    $('.tbsoal').click(function () {
        $('#largeModalLabel').html('Tambah Data');
        $('#action').val('tambah');
        $('#STATUS').val('0');
        $('#tambahData').show();
    });

    $('.dtsoal').click(function () {
        $('#detailModalLabel').html('Detail Data');
        $('#dtID_SOAL').val($(this).data('id_soal'))
        $('#dtSOAL').html($(this).data('soal'))
        $('#dtJWB_A').html($(this).data('jwb_a'))
        $('#dtJWB_B').html($(this).data('jwb_b'))
        $('#dtJWB_C').html($(this).data('jwb_c'))
        $('#dtJWB_D').html($(this).data('jwb_d'))
        $('#dtKUNCI').html($(this).data('kunci'))
        $('#detailData').show()
    });

    $('.edsoal').click(function () {
        $('#largeModalLabel').html('Ubah Data');
        $('#action').val('edit');
        $('#ID_SOAL').val($(this).data('id_soal'))
        $('#SOAL').val($(this).data('soal'))
        $('#JWB_A').val($(this).data('jwb_a'))
        $('#JWB_B').val($(this).data('jwb_b'))
        $('#JWB_C').val($(this).data('jwb_c'))
        $('#JWB_D').val($(this).data('jwb_d'))
        $('#STATUS').val($(this).data('status'))
        $('#tambahData').show();
    });

    $('.edkunci').click(function () {
        $('#largeModalLabel').html('Ubah Kunci');
        $('#action').val('edit');
        $('#ID_SOAL').val($(this).data('id_soal'))
        $('#SOAL').val($(this).data('soal'))
        $('#JWB_A').val($(this).data('jwb_a'))
        $('#JWB_B').val($(this).data('jwb_b'))
        $('#JWB_C').val($(this).data('jwb_c'))
        $('#JWB_D').val($(this).data('jwb_d'))
        $('#STATUS').val($(this).data('status'))
        $('#tambahData').show();
    });    

    $('#submit').click(function (e) {
        e.preventDefault();
        $(this).html('Sending...');
        $.ajax({
            data: $('#formPegawai').serialize(),
            url: "{{ route('tbsoal') }}",
            type: "POST",
            dataType: 'json',
            success: function (data) {
                $('#formPegawai').trigger('reset');
                $('#submit').html('Simpan');
                $('#tambahData').hide();
                console.log('Sukses: ', data);

                Swal.fire({
                    title: 'Success!',
                    text: 'Your data has been updated.',
                    type: 'success',
                    showConfirmButton: true,
                }).then((result) => {
                    if (result.value) {
                        location.reload(true);
                    } else {
                        location.reload(true);
                    }
                });
            },
            error: function (data) {
                console.log('Error: ', data);
            }
        });
    });

    $(document).on('click', '.dlsoal', function (e) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#29A746',
            cancelButtonColor: '#DC3646',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {

                var id = $(this).data('id_soal');

                $.ajax({
                    url: "{{ url('/soal/delete') }}/" + id,
                    type: "GET",
                    dataType: 'json',
                    success: function (data) {
                        console.log('Sukses: ', data);
                    },
                    error: function (data) {
                        console.log('Error: ', data);
                    }
                });

                Swal.fire({
                    title: 'Deleted!',
                    text: 'Your data has been deleted.',
                    type: 'success',
                    showConfirmButton: true,
                }).then((result) => {
                    if (result.value) {
                        location.reload(true);
                    } else {
                        location.reload(true);
                    }
                });
            } else {
                swal.close();
            }
        });
    });
</script>

@endsection