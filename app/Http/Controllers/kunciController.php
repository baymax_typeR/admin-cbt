<?php

namespace App\Http\Controllers;

// use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use Illuminate\Routing\Redirector;

class kunciController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */    

    public function index(Request $request)
    {                                                              
        $kunci = DB::table("kunci as k")
            ->select('k.ID_KUNCI', 'k.ID_SOAL', 'k.KUNCI_JWB',
                    's.ID_SOAL as id_soal', 's.SOAL as soal')
            ->join('soal as s', 'k.ID_SOAL', '=', 's.ID_SOAL')            
            ->get();

        $soal = DB::table("soal")            
            ->get();        
        
        // $str = "&lt;select id=&quot;ID_SOAL&quot; class=&quot;form-control dtjwb&quot; name=&quot;ID_SOAL&quot; onchange=&quot;proses()&quot;&gt;
        //         &lt;option disabled selected&gt;--- Pilih Soal ---&lt;/option&gt;
        //         @foreach($soal)
        //         &lt;option value=&quot; ".$soal[0]->ID_SOAL." &quot;&gt; ".$soal[0]->SOAL." &lt;/option&gt;
        //         @endforeach &lt;/select&gt;";

        // $query = htmlspecialchars_decode($str);

        return view('kunci', ['kunci' => $kunci, 'soal' => $soal]);        
    }

    public function dtsoal($id)
    {                                                                      
        $data = DB::table("soal")
            ->select('ID_SOAL', 'SOAL', 'JWB_A', 'JWB_B', 'JWB_C', 'JWB_D')
            ->where('ID_SOAL', $id)
            ->get();        

        return response()->json($data);
    }
    
    public function tbkunci(Request $request)
    {
        $id = $request->ID_KUNCI;

        $data_array = array(
            'ID_SOAL' => $request->ID_SOAL,
            'KUNCI_JWB' => $request->RADIOS,            
        );

        if ($request->action == "tambah") {
            DB::table('kunci')->insert($data_array); 
        } else {            
            DB::table('kunci')->where('ID_KUNCI', '=', $id)->update($data_array); 
        }

        $data['message'] = "Sukses!";   
        
        return response()->json($data);
    }   

    public function dlkunci(Request $request, $id)
    {   
        $query = DB::table('kunci')->where([
                ['ID_KUNCI', $id],
            ])->delete();

        if ( $query == true ) {
            $data['code'] = "100";
            $data['message'] = "Sukses hapus data kunci jawaban!";
        } else {
            $data['code'] = "404";
            $data['message'] = "Gagal hapus data kunci jawaban!";
        }        
        return response()->json($data);                
    }   
}
