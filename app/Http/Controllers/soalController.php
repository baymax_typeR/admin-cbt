<?php

namespace App\Http\Controllers;

// use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use Illuminate\Routing\Redirector;

class soalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */    

    public function index()
    {                                                              
        $soal = DB::table("soal")            
            ->get();

        return view('soal', ['soal' => $soal]);
    }
    
    public function tbsoal(Request $request)
    {        
        $id = $request->ID_SOAL;
        
        $data_array = array(
            'SOAL' => $request->SOAL,
            'JWB_A' => $request->JWB_A,
            'JWB_B' => $request->JWB_B,
            'JWB_C' => $request->JWB_C,
            'JWB_D' => $request->JWB_D,
            'STATUS' => $request->STATUS,
            'KUNCI' => $request->KUNCI,
        );

        if ($request->action == "tambah") {
            DB::table('soal')->insert($data_array);
        } else {            
            // $data = DB::table("soal")->where('ID_SOAL', $id)->get();
            // $query = DB::table("kunci")->where('ID_SOAL', $id)->get();

            // if ($query[0]->KUNCI_JWB == $data[0]->JWB_A) {
            //     DB::table('kunci')->where('ID_SOAL', '=', $id)->update(['KUNCI_JWB' => $request->JWB_A]);
            // } else if ($query[0]->KUNCI_JWB == $data[0]->JWB_B) {
            //     DB::table('kunci')->where('ID_SOAL', '=', $id)->update(['KUNCI_JWB' => $request->JWB_B]);
            // } else if ($query[0]->KUNCI_JWB == $data[0]->JWB_C) {
            //     DB::table('kunci')->where('ID_SOAL', '=', $id)->update(['KUNCI_JWB' => $request->JWB_C]);
            // } else if ($query[0]->KUNCI_JWB == $data[0]->JWB_D) {
            //     DB::table('kunci')->where('ID_SOAL', '=', $id)->update(['KUNCI_JWB' => $request->JWB_D]);
            // } else {
            //     $data['message'] = "Gagal update!";
            // }
        
            DB::table('soal')->where('ID_SOAL', '=', $id)->update($data_array);
        }

        $data['message'] = "Sukses!";   
        
        return response()->json($data);
    }   

    public function dlsoal(Request $request, $id)
    {   
        $query = DB::table('soal')->where([
                ['ID_SOAL', $id],
            ])->delete();

        if ( $query == true ) {
            $data['code'] = "100";
            $data['message'] = "Sukses hapus data soal!";
        } else {
            $data['code'] = "404";
            $data['message'] = "Gagal hapus data soal!";
        }

        return response()->json($data);                
    }   
}
