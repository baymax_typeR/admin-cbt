<?php

namespace App\Http\Controllers;

// use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use Illuminate\Routing\Redirector;

class ujianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */    

    public function index(Request $request)
    {
        $dataUjian = DB::select("
            select U.ID_UJIAN, U.ID_USER, DTUSER.username, DTUSER.name, U.CREATED_AT from (
                (select ID_UJIAN, ID_USER, CREATED_AT from ujian)U,
                (select id, name, username from users where level = 'user')DTUSER
            ) where U.ID_USER = DTUSER.id
        ");

        $data = array();

        foreach ($dataUjian as $key => $value) {
            $data[$key]['ID_UJIAN']=$value->ID_UJIAN;
            $data[$key]['ID_USER']=$value->ID_USER;
            $data[$key]['username']=$value->username;
            $data[$key]['name']=$value->name;
            $data[$key]['CREATED_AT']=$value->CREATED_AT;
        }
        
        // $data['dataLogin'] = $dataLogin; 
        
        // return response()->json($data);
        return view('ujian', ['data' => $data]);        
    }

    // public function dtsoal($id)
    // {                                                                      
    //     $data = DB::table("soal")
    //         ->select('ID_SOAL', 'SOAL', 'JWB_A', 'JWB_B', 'JWB_C', 'JWB_D')
    //         ->where('ID_SOAL', $id)
    //         ->get();        

    //     return response()->json($data);
    // }
    
    // public function tbkunci(Request $request)
    // {
    //     $id = $request->ID_KUNCI;

    //     $data_array = array(
    //         'ID_SOAL' => $request->ID_SOAL,
    //         'KUNCI_JWB' => $request->RADIOS,            
    //     );

    //     if ($request->action == "tambah") {
    //         DB::table('kunci')->insert($data_array); 
    //     } else {            
    //         DB::table('kunci')->where('ID_KUNCI', '=', $id)->update($data_array); 
    //     }

    //     $data['message'] = "Sukses!";   
        
    //     return response()->json($data);
    // }   

    public function dlujian(Request $request, $id)
    {   
        $query = DB::table('ujian')->where([
                ['ID_UJIAN', $id],
            ])->delete();

        if ( $query == true ) {
            $data['code'] = "100";
            $data['message'] = "Sukses hapus data ujian!";
        } else {
            $data['code'] = "404";
            $data['message'] = "Gagal hapus data ujian!";
        }        
        return response()->json($data);                
    }   
}
